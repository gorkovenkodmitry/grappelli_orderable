from setuptools import setup, find_packages
from os.path import join, dirname

setup(
    name='grappelli_orderable',
    version='0.1.1',
    packages=find_packages(),
    include_package_data=True,
    long_description=open(join(dirname(__file__), 'README.txt')).read(),
)